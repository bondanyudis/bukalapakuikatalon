<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_BukaEmas                Bazar Diskon 60_4f6322</name>
   <tag></tag>
   <elementGuidId>01fd531e-e704-4117-ab94-bd876321dd2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bl-flex-container.mb-0.bl-dope-container__grid-wrapper</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='homepage-home-app']/section/div/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bl-flex-container mb-0 bl-dope-container__grid-wrapper</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>      
      BukaEmas
          
      Bazar Diskon 60%
          
      Pulsa Prabayar
          
      Kartu Prakerja
          
      Paket Data
          
      Voucher Game
          
      Listrik Prabayar
          
      Listrik Pascabayar
          
                  Semua Menu
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;homepage-home-app&quot;)/section[@class=&quot;bl-section dope-section mt-48&quot;]/div[@class=&quot;bl-container&quot;]/div[@class=&quot;qa-dope-container bl-dope-container&quot;]/div[@class=&quot;bl-dope-container__wrapper&quot;]/div[@class=&quot;bl-dope-container__head&quot;]/div[@class=&quot;bl-dope-container__head-content&quot;]/div[@class=&quot;bl-flex-container mb-0 bl-dope-container__grid-wrapper&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='homepage-home-app']/section/div/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua'])[1]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat semua pembelian'])[1]/following::div[68]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua'])[2]/preceding::div[53]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Flash Deal'])[1]/preceding::div[56]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
