import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginSteps {

	@Given("User navigates to login page and click login button")
	def navigateToLoginPage(){
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://www.bukalapak.com/')

		WebUI.click(findTestObject('Object Repository/Page_Situs Belanja Online dan Jual Beli Mud_3c8940/p_Login'))
	}

	@When("User enters valid (.*) and (.*)")
	def enterUsernameAndPassword(String username, String password){

		WebUI.setText(findTestObject('Object Repository/Page_Bukalapak/input_Daftar di sini_user_sessionusername'), 'yudistira96@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Bukalapak/input_Daftar di sini_user_sessionpassword'), 'kJxrqKzTBi66Nkm/D5N4FY/tMYl1+zTo')
	}

	@And("Click on login button")
	def clickButtonLogin(){
		WebUI.click(findTestObject('Object Repository/Page_Bukalapak/i_Lupa Password_c-btn--spinner__icon'))
	}

	@Then("User is navigated to homepage")
	def navigateToHomePage(){
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Situs Belanja Online dan Jual Beli Mud_3c8940/div_BukaEmas                Bazar Diskon 60_4f6322'),
				0)

		WebUI.closeBrowser()
	}
}